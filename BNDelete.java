public boolean delete(int key) {
    Node current = root;
    Node parent = root;
    boolean isLeftChild = true;

    while (current.iData != key) {
        parent = current;
        if (key < current.iData) {
            isLeftChild = true;
            current = current.leftChild;
        } else {
            isLeftChild = false;
            current = current.rightChild;
        }
        if (current == null) {
            return false; // Node with the given key not found
        }
    }
    

    // Found the node to delete

    // if no children, simply delete it
    if (current.leftChild == null && current.rightChild == null) {
        if (current == root) {
            root = null; // if the root node is to be deleted, set root to null to empty the tree
        } else if (isLeftChild) {
            parent.leftChild = null; // disconnect the left child of the parent
        } else {
            parent.rightChild = null; // disconnect the right child of the parent
        }
        return true; // Node with the given key is successfully deleted
    }

    // Handle cases when the node has one child
    if (current.leftChild == null) {
        if (current == root) {
            root = current.rightChild;
        } else if (isLeftChild) {
            parent.leftChild = current.rightChild;
        } else {
            parent.rightChild = current.rightChild;
        }
        return true;
    } else if (current.rightChild == null) {
        if (current == root) {
            root = current.leftChild;
        } else if (isLeftChild) {
            parent.leftChild = current.leftChild;
        } else {
            parent.rightChild = current.leftChild;
        }
        return true;
    }

    // Handle the case when the node has two children
    Node successor = getSuccessor(current);
    if (current == root) {
        root = successor;
    } else if (isLeftChild) {
        parent.leftChild = successor;
    } else {
        parent.rightChild = successor;
    }
    successor.leftChild = current.leftChild;

    return true;
}

private Node getSuccessor(Node node) {
    Node successorParent = node;
    Node successor = node;
    Node current = node.rightChild;

    while (current != null) {
        successorParent = successor;
        successor = current;
        current = current.leftChild;
    }

    // If the successor is not the right child of the node to be deleted,
    // rearrange the pointers
    if (successor != node.rightChild) {
        successorParent.leftChild = successor.rightChild;
        successor.rightChild = node.rightChild;
    }

    return successor;
}
