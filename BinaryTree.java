class Node {
    public int iData; // data item (key)
    public double dData; // data item
    public Node leftChild; // reference to left child
    public Node rightChild; // reference to right child

    public void displayNode() {
        System.out.print("{" + iData + ", " + dData + "} ");
    }
}

class Tree {
    private Node root; // the only data field in Tree

    public Node find(int key) {
        Node current = root;
        while (current != null && current.iData != key) {
            if (key < current.iData) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
        }
        return current;
    }

    public void insert(int id, double dd) {
        Node newNode = new Node();
        newNode.iData = id;
        newNode.dData = dd;
        if (root == null) {
            root = newNode;
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (id < current.iData) {
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public void delete(int id) {
        // You can implement delete method if needed.
    }
    
    // Other methods for tree operations
}
class TreeApp {
    public static void main(String[] args) {
        Tree theTree = new Tree(); // make a tree
        theTree.insert(50, 1.5); // insert 3 nodes
        theTree.insert(25, 1.7);
        theTree.insert(75, 1.9);
        Node found = theTree.find(25); // find node with key 25
        if (found != null)
            System.out.println("Found the node with key 25");
        else
            System.out.println("Could not find node with key 25");
    }
}