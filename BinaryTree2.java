class Node {
    public int iData; // data item (key)
    public double dData; // data item
    public Node leftChild; // reference to left child
    public Node rightChild; // reference to right child

    public void displayNode() {
        System.out.print("{" + iData + ", " + dData + "} ");
    }
}

class Tree {
    private Node root; // the only data field in Tree

    public void insert(int id, double dd) {
        Node newNode = new Node();
        newNode.iData = id;
        newNode.dData = dd;
        if (root == null) {
            root = newNode;
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (id < current.iData) {
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public Node find(int key) {
        Node current = root;
        while (current != null && current.iData != key) {
            if (key < current.iData) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
        }
        return current;
    }

    public void delete(int id) {
        // Implement delete method if needed.
    }
    
    // Other methods for tree operations
}
